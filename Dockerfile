FROM debian:jessie

MAINTAINER  J.P.C. Oudeman

##
# Might want to map the sourcefiles to /var/lib/node/app
##

COPY assets/bin/entrypoint.sh /usr/local/bin

# Install node
RUN apt-get update && \
 apt-get install -y curl && \
 curl -sL https://deb.nodesource.com/setup_4.x | bash - && \
 apt-get install -y nodejs && \
\
# prepare environment
 mkdir -p /var/lib/node/app /var/www && \
 chown www-data: /var/lib/node/app /var/www && \
 chmod +x /usr/local/bin/*.sh && \
 rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

USER www-data

ENTRYPOINT ["entrypoint.sh"]
CMD ["npm", "start"]
ENV WORKDIR /var/lib/node/app
WORKDIR $WORKDIR