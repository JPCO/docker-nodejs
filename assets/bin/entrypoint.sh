#!/bin/sh
cd $WORKDIR
echo starting node
if [ ! -d "node_modules" ]; then
	npm install
fi

exec "$@"
